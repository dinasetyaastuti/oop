<?php
require ('animal.php');
require ('Ape.php');

$sheep = new Animal("shaun");
echo "Nama : " . $sheep->name . "<br>"; // "shaun"
echo "Jumlah kaki :" . $sheep->legs . "<br>"; // 2
echo "Cold Blood : "; 
var_dump($sheep->cold_blooded) . "<br>";// false
echo "<br><br>";
$ape = new Ape ("Ape");
echo "Nama : " . $ape->name . "<br>";
echo "Jumlah kaki :" . $ape->legs . "<br>";
echo "Cold Blood : ";
var_dump($ape->cold_blooded);
echo "<br>";
echo "Bunyi :";
echo $ape->yell();
?>